from django.db import models
import datetime
from django.utils import timezone

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=400)
    pub_date = models.DateTimeField(auto_now_add=True)
    due_date = models.DateField('due date')
    modification_date = models.DateField(auto_now=True)

    
    def __str__(self):
        return self.name

    # def was_published_recently(self):
    #   return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
        