from django.conf.urls import url

from views import index, add_task, task_list

urlpatterns = [
    url(r'^$', index, name='index'),
	url(r'^add/$', add_task, name='add_task'),
	url(r'^tasks/$', task_list, name='tasks'),
]

