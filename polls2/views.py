from django.shortcuts import render
from django.http import HttpResponse

from .models import Task
from .forms import FormTaskAdd

def index(request):
	
    return render(request, 'polls2/index.html')

def add_task(request):
    if request.method == 'POST':
        form = FormTaskAdd(request.POST)
        if form.is_valid():
            # task = form.save(commit=False)
            form.save()
            return render(request, 'polls2/task_list.html')
    else:        
        form = FormTaskAdd()
    return render(request, 'polls2/add_task.html', {'form':form})

def task_list(request):
    tasks = Task.objects.all()
    return render(request, 'polls2/task_list.html', {'task_list':tasks})